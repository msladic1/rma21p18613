//package ba.etf.rma21.projekat
//
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso
//import androidx.test.espresso.action.ViewActions
//import androidx.test.espresso.assertion.ViewAssertions
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.intent.rule.IntentsTestRule
//import androidx.test.espresso.matcher.ViewMatchers
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma21.projekat.UtilTestClass.Companion.hasItemCount
//import ba.etf.rma21.projekat.UtilTestClass.Companion.itemTest
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import ba.etf.rma21.projekat.data.repositories.PredmetRepository
//import org.hamcrest.CoreMatchers
//import org.hamcrest.Matchers
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//    @get:Rule
//    val intentsTestRule = IntentsTestRule<MainActivity>(MainActivity::class.java)
//
//    @Test
//    fun prviZadatak(){  //Test provjerava da li je upis omogućen tek nakon što korisnik odabere sva polja u fragmentu predmeti, a zatim
//                        //provjerava da li pritisak na back dugme vraća na fragment kvizovi
//        Espresso.onView(ViewMatchers.withId(R.id.predmeti)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGodina)).perform(ViewActions.click())
//        val nedodjeljeniKvizovi = KvizRepository.getAll().minus(KvizRepository.getMyKvizes())
//        val nedodjeljeniPredmeti = PredmetRepository.getAll().minus(PredmetRepository.getUpisani())
//
//        var grupaVrijednost = ""
//        var predmetNaziv = ""
//        var godinaVrijednost = -1
//        for (nk in nedodjeljeniKvizovi) {
//            for (np in nedodjeljeniPredmeti) {
//                if (nk.nazivPredmeta == np.naziv) {
//                    grupaVrijednost = nk.nazivGrupe
//                    godinaVrijednost = np.godina
//                    predmetNaziv = np.naziv
//
//                }
//            }
//        }
//
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(godinaVrijednost.toString()))).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.dodajPredmetDugme)).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.odabirPredmet)).perform(ViewActions.click())
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(predmetNaziv))).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.dodajPredmetDugme)).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.odabirGrupa)).perform(ViewActions.click())
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
//                        CoreMatchers.`is`(grupaVrijednost))).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withId(R.id.dodajPredmetDugme)).perform(ViewActions.click())
//
//        Espresso.onView(ViewMatchers.withSubstring("Uspješno ste upisani u grupu"))
//
//        Espresso.pressBack()
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova))
//    }
//
//    @Test
//    fun drugiZadatak() { //Test provjerava da li se nakon povratka na predani (završeni) kviz u meniju pojavljuje polje rezultat
//                         //i da li klik na tu opciju otvara poruku "Završili ste kviz..."
//        Espresso.onView(ViewMatchers.withId(R.id.filterKvizova)).perform(ViewActions.click())
//        Espresso.onData(CoreMatchers.allOf(CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)), CoreMatchers.`is`("Svi moji kvizovi"))).perform(ViewActions.click())
//        val kvizovi = KvizRepository.getMyKvizes()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(ViewMatchers.hasDescendant(ViewMatchers.withText(kvizovi[1].naziv)),
//                ViewMatchers.hasDescendant(ViewMatchers.withText(kvizovi[1].nazivPredmeta))), ViewActions.click()))
//        Espresso.onView(ViewMatchers.withId(R.id.navigacijaPitanja)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//        Espresso.onView(ViewMatchers.withId(R.id.predajKviz)).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withSubstring("Završili ste kviz")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//        Espresso.pressBack()
//        Espresso.onView(ViewMatchers.withId(R.id.listaKvizova)).perform(RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(CoreMatchers.allOf(ViewMatchers.hasDescendant(ViewMatchers.withText(kvizovi[1].naziv)),
//                ViewMatchers.hasDescendant(ViewMatchers.withText(kvizovi[1].nazivPredmeta))), ViewActions.click()))
//
//        Espresso.onView(ViewMatchers.withText("Rezultat")).perform(ViewActions.click())
//        Espresso.onView(ViewMatchers.withSubstring("Završili ste kviz")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
//
//    }
//}