//package ba.etf.rma21.projekat.data.models
//
//fun sveGrupe(): List<Grupa> {
//    return listOf(
//        Grupa("MPS1-1", "MPS"),
//        Grupa("MPS1-2", "MPS"),
//        Grupa("PŠP1-1", "PŠP"),
//        Grupa("PŠP1-2", "PŠP"),
//        Grupa("VIS1-1", "VIS"),
//        Grupa("VIS1-2", "VIS"),
//        Grupa("IF11-1", "IF1"),
//        Grupa("IF11-2", "IF1"),
//        Grupa("PAD2-1", "PAD"),
//        Grupa("PAD2-2", "PAD"),
//        Grupa("IEK2-1", "IEK"),
//        Grupa("IEK2-2", "IEK"),
//        Grupa("VPM3-1", "VPM"),
//        Grupa("VPM3-2", "VPM"),
//        Grupa("RG3-1", "RG"),
//        Grupa("RG3-2", "RG"),
//        Grupa("SS4-1", "SS"),
//        Grupa("SS4-2", "SS"),
//        Grupa("VČ4-1", "VČ"),
//        Grupa("VČ4-2", "VČ"),
//        Grupa("PC5-1", "PC"),
//        Grupa("PC5-2", "PC"),
//        Grupa("PKS5-1", "PKS"),
//        Grupa("PKS5-2", "PKS")
//    )
//}