package ba.etf.rma21.projekat.data.models

import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.http.*
import java.util.*

interface Api {
    @GET("/predmet")
    suspend fun getAllPredmeti() : List<Predmet>

    @GET ("/grupa")
    suspend fun getAllGrupa() : List<Grupa>

    @GET("/predmet/{id}/grupa")
    suspend fun getGrupaZaPredmet(@Path("id") predmetId: Int): List<Grupa>

    @GET("/kviz/{id}/grupa")
    suspend fun getGrupaZaKviz(@Path("id") kvizId: Int): List<Grupa>

    @GET("/kviz")
    suspend fun getAllKviz() : List<Kviz>

    @GET("/kviz/{id}")
    suspend fun getKvizById(@Path("id") kvizId: Int): Kviz

    @POST("/grupa/{gid}/student/{id}")
    suspend fun  upisiUGrupu(@Path("gid") grupaID: Int, @Path("id") studentId: String): Observable

    @GET("/student/{id}/grupa")
    suspend fun getMojeGrupe(@Path("id") studentId: String): List<Grupa>

    @GET("/kviz/{id}/pitanja")
    suspend fun getPitanjZakViz(@Path("id") kvizId: Int): List<Pitanje>?

    @POST("/student/{id}/kviz/{kid}")
    suspend fun zapocniKviz(@Path("id") studentId: String, @Path("kid") kvizId: Int): KvizTaken?

    @GET("/student/{id}/kviztaken")
    suspend fun getPocetiKvizovi(@Path("id") studentId: String): List<KvizTaken>

    @POST("/student/{id}/kviztaken/{ktid}/odgovor")
    suspend fun postaviOdgovorKviz(@Path("id") studentId: String, @Path("ktid") kvizTakenId: Int,
                                   @Body zahtjev: RequestBody): Observable

    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getOdgovori(@Path("id") studnetId: String, @Path("ktid") kvizTakenId: Int): List<Odgovor>

    @GET("/account/{id}/lastUpdate")
    suspend fun getUpdate(@Path("id") id:String, @Query("date") date:String): Message
}

