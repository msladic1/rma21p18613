package ba.etf.rma21.projekat.data.models

import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.viewmodel.scope
import kotlinx.coroutines.launch

var mojiPredmeti: MutableList<Predmet> = listOf( Predmet (11,"MPS",1), Predmet(12,"PAD", 2),  Predmet(13,"IF1", 1), Predmet(14,"PŠP",1)).toMutableList()

var posljednjaGodina: String = "1"
var posljednjiPredmet: String = "VIS"
var posljednjaGrupa: String = "VIS1-1"

fun sviPredmeti(): List<Predmet> {
    return listOf(
        Predmet(11,"MPS",1),
        Predmet(12,"PŠP",1),
        Predmet(13,"VIS", 1),
        Predmet(14,"IF1", 1),
        Predmet(15,"PAD",2),
        Predmet(16,"IEK", 2),
        Predmet(17,"VPM", 3),
        Predmet(18,"RG", 3),
        Predmet(19,"SS", 4),
        Predmet(20,"VČ", 4),
        Predmet(21,"PC",5),
        Predmet(22,"PKS",5)
    )
}

fun mojiPredmeti(predmet: String =""): List<Predmet> {
    scope.launch {
        val pom: List<Predmet> = PredmetIGrupaRepository.getPredmeti().filter { mojpredmet -> mojpredmet.naziv.equals(predmet) }
        for(odabrani: Predmet in pom)
            mojiPredmeti.add(odabrani)
    }
    return mojiPredmeti
}

fun izmjeniPosljednjuGodinu(nova: String) {
    posljednjaGodina = nova
}

fun vratiPosljednjuGodinu(): String {
    return posljednjaGodina
}

fun izmjeniPosljednjiPredmet(nova: String) {
    posljednjiPredmet = nova
}

fun vratiPosljednjiPredmet(): String {
    return posljednjiPredmet
}

fun izmjeniPosljednjuGrupu(nova: String) {
    posljednjaGrupa = nova
}

fun vratiPosljednjuGrupu(): String {
    return posljednjaGrupa
}
