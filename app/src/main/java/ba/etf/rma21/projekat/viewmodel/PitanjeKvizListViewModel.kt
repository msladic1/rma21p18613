package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import kotlinx.coroutines.launch

class PitanjeKvizListViewModel {
    suspend fun getPitanja(idKviza: Int): List<Pitanje>? {
        return PitanjeKvizRepository.getPitanja(idKviza)
    }

    fun dodajZapocetiKviz(kviz: PitanjeKviz, pozicijaTacnost: HashMap<Int, Boolean>) {
        PitanjeKvizRepository.dodajZapocetiKvi(kviz, pozicijaTacnost)
    }

    fun postaviTrenutni(kviz: String){
        PitanjeKvizRepository.postaviTrenutni(kviz)
    }

    fun postaviTrenutniId(id: Int){
        trenutniKvizId = id
    }

    fun postaviTrenutniPred(predmet: String?) {
        PitanjeKvizRepository.postaviTrenutniPred(predmet)
    }

    fun predajKviz(){
        PitanjeKvizRepository.predajKviz()
    }

    fun dodajPredanKviz(kviz: PitanjeKviz){
        PitanjeKvizRepository.dodajPredanKviz(kviz)
    }

    fun dodajTacni(){
        PitanjeKvizRepository.dodajTacni()
    }

    suspend fun zapocniKviz(kvizId : Int): KvizTaken?{
        return TakeKvizRepository.zapocniKviz(kvizId)
    }

    suspend fun postaviOdgovor(kvizTakenId: Int, idPitanje: Int, odgovor: Int){
        OdgovorRepository.postaviOdgovorKviz(kvizTakenId, idPitanje, odgovor)
    }
    fun predajOdgovore(idKviz: Int){
        scope.launch {
            OdgovorRepository.predajOdgovore(idKviz)
        }
    }
}