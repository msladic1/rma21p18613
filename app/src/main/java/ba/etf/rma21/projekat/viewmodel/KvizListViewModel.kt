package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.*
import kotlinx.coroutines.launch
import java.util.*

class KvizListViewModel {

     @RequiresApi(Build.VERSION_CODES.O)
     fun getMyKvizes1(context: Context, onSuccess: (kvizovi: List<Kviz>) -> Unit) {
         scope.launch {
             var result = listOf<Kviz>()
             trenutniKorisnik = AccountRepository.getAccountIzBaze(context)!!
              if(DBRepository.updateNow()!!) {
                  result = KvizRepository.getMyKvizes1().sortedBy { kviz -> kviz.datumPocetka }
                  AccountRepository.postaviVrijemeZadnjegUpdate(context, trenutniKorisnik)
                  var grupe = PredmetIGrupaRepository.getUpisaneGrupe()
                 for(kviz in result) {
                     KvizRepository.upisiKvizUBazu(context, kviz)
                 }
                  var predmeti = PredmetIGrupaRepository.getPredmeti()
                  for(grupa in grupe!!){
                      PredmetIGrupaRepository.upisiGrupuUBazu(context, grupa)
                      for(predmet in predmeti){
                          if(grupa.nazivPredmeta == predmet.naziv)
                              PredmetIGrupaRepository.upisiPredmetUBazu(context, predmet)
                      }
                  }
             } else {
                  result = KvizRepository.getKvizoveIzBaze(context)!!
                  if(result.isEmpty()) {
                      result = KvizRepository.getMyKvizes1().sortedBy { kviz -> kviz.datumPocetka }
                      for(kviz in result)
                          KvizRepository.upisiKvizUBazu(context, kviz)
                  }
                  //result = listOf(Kviz(0,"test","test", "2021-05-20", null, null, 15 , "Grupa 8", null))
             }
             when(result){
                 else -> onSuccess?.invoke(result)
             }
         }
    }

    fun getAll(onSuccess: (kvizovi: List<Kviz>) -> Unit){
        scope.launch {
            val result = KvizRepository.getAll().sortedBy { kviz -> kviz.datumPocetka }
            when(result){
                else -> onSuccess?.invoke(result)
            }
        }
    }

     fun getDone(context: Context, onSuccess: (kvizovi: List<Kviz>) -> Unit){
         scope.launch {
             val result = KvizRepository.getIzBazeDone(context)!!.sortedBy { kviz -> kviz.datumPocetka }
             when(result){
                 else -> onSuccess?.invoke(result)
             }
         }
    }

     @RequiresApi(Build.VERSION_CODES.O)
     fun getFuture(context: Context, onSuccess: (kvizovi: List<Kviz>) -> Unit) {
         scope.launch {
             val result = KvizRepository.getIzBazeFuture(context)!!.sortedBy { kviz -> kviz.datumPocetka }
             when(result){
                 else -> onSuccess?.invoke(result)
             }
         }
    }

     @RequiresApi(Build.VERSION_CODES.O)
     fun getNotTaken(context: Context, onSuccess: (kvizovi: List<Kviz>) -> Unit) {
         scope.launch {
             val result = KvizRepository.getIzBazeNotTaken(context)!!.sortedBy { kviz -> kviz.datumPocetka }
             when(result){
                 else -> onSuccess?.invoke(result)
             }
         }
    }

    fun setMyKvizes(grupa: String){
        scope.launch {
            KvizRepository.setMyKvizes(grupa)
        }
    }

     fun getUpisani(): List<Kviz> {
        var a = mutableListOf<Kviz>()
        scope.launch {
            a = KvizRepository.getUpisani().toMutableList()
        }
         return a
    }

    fun azurirajKviz(context: Context, idKviz: Int) {
        scope.launch {
            KvizRepository.azurirajKviz(context, idKviz)
        }
    }

    suspend fun getZapoceti(): List<KvizTaken>? {
        if(TakeKvizRepository.getPocetiKvizovi() != null)
            return TakeKvizRepository.getPocetiKvizovi()!!
        return emptyList()
    }

    /*fun getJedanZapoceti(onSuccess: (kvizovi: List<KvizTaken>) -> Unit, idKvizTaken: Int) {
        scope.launch {
            val result = TakeKvizRepository.getPocetiKvizovi()!!.filter { poceti -> poceti.id == idKvizTaken }
            when(result){
                else -> onSuccess?.invoke(result)
            }
        }
    }*/
    fun getOdgovoriNaKvizu(onSuccess: (kvizovi: List<Odgovor>) -> Unit, idKvizaTaken: Int){
        scope.launch {
            val result = OdgovorRepository.getOdgovoriKviz(idKvizaTaken)!!
            when(result){
                else -> onSuccess?.invoke(result)
            }
        }
    }
}