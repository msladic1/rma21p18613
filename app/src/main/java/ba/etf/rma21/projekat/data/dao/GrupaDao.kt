package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa

@Dao
interface GrupaDao {
    @Query("SELECT * FROM grupa")
    suspend fun getUpisane(): List<Grupa>
    @Insert
    suspend fun insertGrupa(vararg grupa: Grupa)
    @Query("DELETE FROM grupa")
    suspend fun deleteGrupe()
}