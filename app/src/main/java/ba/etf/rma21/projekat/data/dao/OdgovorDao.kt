package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDao {
    @Query("SELECT * FROM odgovor WHERE odgovor.KvizId == :kvizeId")
    suspend fun getOdgovorene(kvizeId: Int): List<Odgovor>
    @Insert
    suspend fun insertOdgovor(vararg odgovori: Odgovor)
    @Query("DELETE FROM odgovor")
    suspend fun deleteAll()
}