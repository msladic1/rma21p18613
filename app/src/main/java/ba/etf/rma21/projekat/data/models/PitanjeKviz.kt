package ba.etf.rma21.projekat.data.models

class PitanjeKviz (val naziv: String, val kviz: String, val predmet: String?) {

    override fun toString(): String {
        return "$naziv $kviz $predmet"
    }

    override fun equals(other: Any?): Boolean {
        if(other is PitanjeKviz && other.naziv == naziv && other.kviz == kviz && other.predmet == predmet)
        return true
        return false
    }
}