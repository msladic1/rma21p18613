package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PitanjaOdgovoriViewModel: ViewModel() {
    val predan = MutableLiveData<Boolean>()

    fun predaj(){
        predan.value = true
    }
}
