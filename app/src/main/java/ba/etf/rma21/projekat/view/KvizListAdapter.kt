package ba.etf.rma21.projekat.view

import android.annotation.SuppressLint
import android.content.Context
import android.icu.util.Calendar
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.viewmodel.scope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*


class KvizListAdapter(private var kvizovi: List<Kviz>, private val onItemClicked: (kviz:Kviz) -> Unit) : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_kviz, parent, false)
        return KvizViewHolder(view)
    }

    override fun getItemCount(): Int = kvizovi.size

    val sdf = SimpleDateFormat("yyyy-mm-dd")
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
        scope.launch {
            holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta
            holder.imeKviza.text = kvizovi[position].naziv
            if (kvizovi[position].osvojeniBodovi != null) {
                /*var cal: Calendar = Calendar.getInstance()
                cal.setTime(kvizovi[position].datumRada)*/
                holder.datumKviza.text = kvizovi[position].datumRada//cal.get(Calendar.DAY_OF_MONTH).toString() + "." + (cal.get(Calendar.MONTH) + 1).toString() + "." + cal.get(Calendar.YEAR)
            } else if (kvizovi[position].datumPocetka > Calendar.getInstance().time.toString()) {
                /*var cal: Calendar = Calendar.getInstance()
                cal.setTime(kvizovi[position].datumPocetka)*/
                holder.datumKviza.text = kvizovi[position].datumPocetka//cal.get(Calendar.DAY_OF_MONTH).toString() + "." + (cal.get(Calendar.MONTH) + 1).toString() + "." + cal.get(Calendar.YEAR)
            } else {
                /*var cal: Calendar = Calendar.getInstance()
                cal.setTime(kvizovi[position].datumPocetka)*/
                holder.datumKviza.text = kvizovi[position].datumPocetka//cal.get(Calendar.DAY_OF_MONTH).toString() + "." + (cal.get(Calendar.MONTH) + 1).toString() + "." + cal.get(Calendar.YEAR)
            }

            holder.trajanjeKviza.text = kvizovi[position].trajanje.toString() + "min"

            if (kvizovi[position].osvojeniBodovi != null)
                holder.ostvareniBodovi.text = kvizovi[position].osvojeniBodovi.toString()
            else if (kvizovi[position].osvojeniBodovi == null)
                holder.ostvareniBodovi.text = ""


            val sdf = SimpleDateFormat("yyyy-mm-dd")
            val context: Context = holder.oznakaKviza.getContext()
            var id: Int = 0
            if (kvizovi[position].osvojeniBodovi != null)
                id = context.getResources()
                    .getIdentifier("plava", "drawable", context.getPackageName())
            else if ((kvizovi[position].datumKraj == null || sdf.parse(kvizovi[position].datumKraj!!) > Date()) && sdf.parse(kvizovi[position].datumPocetka) < Date())
                id = context.getResources()
                        .getIdentifier("zelena", "drawable", context.getPackageName())
            else if (kvizovi[position].datumKraj == null || sdf.parse(kvizovi[position].datumKraj!!) < Date())
                id = context.getResources()
                    .getIdentifier("crvena", "drawable", context.getPackageName())
            else if (sdf.parse(kvizovi[position].datumPocetka) > Date())
                id = context.getResources()
                        .getIdentifier("zuta", "drawable", context.getPackageName())

            holder.oznakaKviza.setImageResource(id)

            holder.itemView.setOnClickListener { onItemClicked(kvizovi[position]) }
        }
    }

    fun updateKvizovi(kvizovi: List<Kviz>) {
        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }
    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nazivPredmeta: TextView = itemView.findViewById(R.id.nazivPredmeta)
        val imeKviza: TextView = itemView.findViewById(R.id.imeKviza)
        val datumKviza: TextView = itemView.findViewById(R.id.datumKviza)
        val trajanjeKviza: TextView = itemView.findViewById(R.id.trajanjeKviza)
        val ostvareniBodovi: TextView = itemView.findViewById(R.id.ostvareniBodovi)
        val oznakaKviza: ImageView = itemView.findViewById(R.id.kvizStatus)
    }
}