package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class TakeKvizRepository {

    companion object {
       suspend fun zapocniKviz(idKviza:Int): KvizTaken? {
           return withContext(Dispatchers.IO) {
               try {
                   var response = ApiAdapter.retrofit.zapocniKviz(AccountRepository.acHash, idKviza)
                   response!!.KvizId = idKviza
                   return@withContext response
               }catch(e: Exception) {
                   return@withContext null
               }
           }
    }

    suspend fun getPocetiKvizovi():List<KvizTaken>? {
        return withContext(Dispatchers.IO) {
            var response = ApiAdapter.retrofit.getPocetiKvizovi(AccountRepository.acHash)
            if(response.isEmpty())
                return@withContext null
            return@withContext response
        }
    }
   }
}