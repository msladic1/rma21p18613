package ba.etf.rma21.projekat.view

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.view.KvizListAdapter
import ba.etf.rma21.projekat.viewmodel.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FragmentKvizovi: androidx.fragment.app.Fragment() {
    private lateinit var kvizovi: RecyclerView
    private lateinit var sviKvizoviAdapter: KvizListAdapter
    private var kvizListViewModel = KvizListViewModel()
    private var pitanjeKvizListViewModel = PitanjeKvizListViewModel()
    private var accountListViewModel = AccountListViewModel()
    private lateinit var spinner: Spinner
    private lateinit  var pokusajMainViewModel: PokusajMainViewModel

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  {
        var view = inflater.inflate(R.layout.kvizovi_fragment, container, false)

        spinner = view.findViewById(R.id.filterKvizova)
        kvizovi = view.findViewById(R.id.listaKvizova)
        kvizovi.layoutManager = GridLayoutManager(activity, 2)

        sviKvizoviAdapter = KvizListAdapter(arrayListOf()) {kviz -> zapocniPokusaj(kviz)}
        kvizovi.adapter = sviKvizoviAdapter

        /*context?.let {
            accountListViewModel.getAccount(AccountRepository.getContext())
            if(trenutniKorisnik.acHash != AccountRepository.acHash) {
                accountListViewModel.obrisiSve(it)
                trenutniKorisnik.acHash = AccountRepository.acHash
            }
            //accountListViewModel.postaviAccountUBazu(it, trenutniKorisnik)
            trenutniContext = it
        }*/

        context?.let {
            kvizListViewModel.getMyKvizes1(it,onSuccess = ::onSuccess)
        }

        pokusajMainViewModel = ViewModelProviders.of(this)[PokusajMainViewModel::class.java]


        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                context?.let {
                    if (parent.getItemAtPosition(position).toString().equals("Svi kvizovi"))
                        kvizListViewModel.getAll(::onSuccess)
                    else if (parent.getItemAtPosition(position).toString().equals("Urađeni kvizovi"))
                        kvizListViewModel.getDone(it, ::onSuccess)
                    else if (parent.getItemAtPosition(position).toString().equals("Budući kvizovi"))
                        kvizListViewModel.getFuture(it, ::onSuccess)
                    else if (parent.getItemAtPosition(position).toString().equals("Prošli kvizovi"))
                        kvizListViewModel.getNotTaken(it, ::onSuccess)
                    else if (parent.getItemAtPosition(position).toString().equals("Svi moji kvizovi"))
                        kvizListViewModel.getMyKvizes1(it,onSuccess = ::onSuccess)
                }

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        ArrayAdapter.createFromResource(
            activity!!.baseContext,
            R.array.vrste_kvizova,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        return view
    }

    fun onSuccess(kvizevi: List<Kviz>){
        GlobalScope.launch (Dispatchers.IO){
            withContext(Dispatchers.Main){
                sviKvizoviAdapter.updateKvizovi(kvizevi)
            }
        }
    }

    fun zapocniPokusaj(kviz: Kviz) {
        scope.launch {
            val pom = pitanjeKvizListViewModel.getPitanja(kviz.id)
            //pitanjeKvizListViewModel.dodajZapocetiKviz(kviz.naziv, kviz.nazivPredmeta)
            pitanjeKvizListViewModel.postaviTrenutni(kviz.naziv)
            pitanjeKvizListViewModel.postaviTrenutniId(kviz.id)
            pitanjeKvizListViewModel.postaviTrenutniPred(kviz.nazivPredmeta)
            var svizapoceti = kvizListViewModel.getZapoceti()!!.filter { kvizTaken -> kvizTaken.KvizId == kviz.id }
            var pokusaj = FragmentPokusaj(null, 0)
            if(svizapoceti.isEmpty()) {
                var a = pitanjeKvizListViewModel.zapocniKviz(kviz.id)
                 pokusaj = FragmentPokusaj(pitanjeKvizListViewModel.getPitanja(kviz.id), a!!.id)
            }
            else{
                 pokusaj = FragmentPokusaj(pitanjeKvizListViewModel.getPitanja(kviz.id), svizapoceti.first().id)
            }

            val predmetKviz: MutableList<PitanjeKviz> = emptyList<PitanjeKviz>().toMutableList()
            for (pitanje: Pitanje in pom!!) {
                predmetKviz.add(PitanjeKviz(pitanje.naziv, kviz.naziv, kviz.naziv))
            }

            if (predaniKvizovi.containsAll(predmetKviz)) {
                pokusajMainViewModel.omoguciMain()
            } else {
                pokusajMainViewModel.omoguciMain()
            }

            val transaction = fragmentManager!!.beginTransaction()
            transaction.replace(R.id.container, pokusaj)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }
    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

}