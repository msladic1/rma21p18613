package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.BuildConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

object ApiConfig {
    var baseURL : String = "https://rma21-etf.herokuapp.com"

    suspend  fun postaviBaseURL(baseUrl:String):Unit {
        return withContext(Dispatchers.IO) {
            baseURL = baseUrl
        }
    }
}