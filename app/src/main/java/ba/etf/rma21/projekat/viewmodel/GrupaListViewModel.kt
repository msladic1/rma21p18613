package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.sveGrupe
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository

class GrupaListViewModel {
    /*fun getGroupsByPredmet(nazivPredmeta: String): List<Grupa> {
        return GrupaRepository.getGroupsByPredmet(nazivPredmeta)
    }*/
    suspend fun getGroupsByPredmet(idPredmeta: Int): List<Grupa> {
        var a : List<Grupa> = PredmetIGrupaRepository.getGrupeZaPredmet(idPredmeta)
        return a
    }
}