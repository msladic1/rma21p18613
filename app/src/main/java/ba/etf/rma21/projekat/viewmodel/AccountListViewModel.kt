package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.trenutniKorisnik
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import kotlinx.coroutines.launch

class AccountListViewModel {
    fun postaviHash(acHash:String){
        scope.launch {
           AccountRepository.postaviHash(acHash)
        }
    }

    fun postaviStariHash(acHash:String){
        scope.launch {
            AccountRepository.postaviStariHash(acHash)
        }
    }

    fun postaviContext(context: Context){
        AccountRepository.setContext(context)
    }

    fun getAccount(context: Context){
        scope.launch {
            trenutniKorisnik = AccountRepository.getAccountIzBaze(context)!!
        }
    }

    fun postaviAccountUBazu(context: Context, account: Account?) {
        scope.launch {
            AccountRepository.postaviAccountUBazu(context, account)
        }
    }

    fun obrisiSve(context: Context) {
        scope.launch {
            AccountRepository.obrisiSve(context)
        }
    }
}