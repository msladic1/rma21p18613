package ba.etf.rma21.projekat.data.models

var zapocetiKvizovi: MutableMap<PitanjeKviz, HashMap<Int, Boolean>> = HashMap()
var trenutniKviz: String = ""
var trenutniKvizId: Int = 0
var trenutniPredmet: String? = ""
var predaniKvizovi: MutableList<PitanjeKviz> = emptyList<PitanjeKviz>().toMutableList()
var predan: Boolean = false
var tacni: Int = 0


fun dodajZapocetiKviz(kviz: PitanjeKviz, pozicijaTacnost: HashMap<Int, Boolean>) {
    zapocetiKvizovi[kviz] = pozicijaTacnost
}

fun postaviTrenutniKviz(kviz: String) {
    trenutniKviz = kviz
}

fun postaviTrenutniPredmet(predmet: String?) {
    trenutniPredmet = predmet
}

fun dodajPredaniKviz(kviz: PitanjeKviz){
    predaniKvizovi.add(kviz)
}

fun predaj(){
    predan = true
}

fun povecaj() {
    tacni++
}
