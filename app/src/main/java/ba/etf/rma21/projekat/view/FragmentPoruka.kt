package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.predan

class FragmentPoruka : Fragment() {
    private var poruka: String = "Uspješno ste upisani u grupu "
    private var poruka2: String = "Završili ste kviz "
    private lateinit var tvPoruka: TextView
    private lateinit var grupa: String
    private lateinit var predmet: String
    private lateinit var kviz: String
    private lateinit var uspjesnost: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        grupa = arguments!!.getString("grupa").toString()
        predmet = arguments!!.getString("predmet").toString()
        poruka += grupa + " predmeta " + predmet
        kviz = arguments!!.getString("kviz").toString()
        //uspjesnost = arguments!!.getString("tacni").toString()

        poruka2 += "$kviz sa tačnosti "//$uspjesnost%"

        var view = inflater.inflate(R.layout.poruka_fragment, container, false)
        tvPoruka = view.findViewById(R.id.tvPoruka)
        if(poruka2.contains("null"))
            tvPoruka.text = poruka
        else
            tvPoruka.text = poruka2
        return view
    }
}