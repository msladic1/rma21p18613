package ba.etf.rma21.projekat.viewmodel

import android.content.Context
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.*

val scope = CoroutineScope(Job() + Dispatchers.Main)

class PredmetListViewModel (){
    fun getAll():List<Predmet>{
        lateinit var predmeti :List<Predmet>
        scope.launch {
            predmeti = PredmetIGrupaRepository.getPredmeti();
        }
        return predmeti
    }
     suspend fun getPredmetByGodina(godina: Int): List<Predmet> {

         var predmeti:List<Predmet> = PredmetIGrupaRepository.getPredmeti()
            predmeti = predmeti.filter { predmet -> predmet.godina == godina }

        return predmeti
        //return PredmetRepository.getPredmetByGodina(godina)
    }

    fun getPredmetByNaziv(naziv: String): List<Predmet> {
        return PredmetRepository.getPredmetByNaziv(naziv)
    }

    fun getUpisani(): List<Predmet> {
        return PredmetRepository.getUpisani()
    }

    fun setMyPredmeti(naziv: String){
        PredmetRepository.setMyPredmeti(naziv)
    }

    fun odaberiGodinu(godina: String) {
        PredmetRepository.odaberiGodinu(godina)
    }

    fun vratiGodinu(): String{
        return PredmetRepository.vratiGodinu()
    }

    fun odaberiPredmet(predmet: String) {
        PredmetRepository.odaberiPredmet(predmet)
    }

    fun vratiPredmet(): String{
        return PredmetRepository.vratiPredmet()
    }

    fun odaberiGrupu(predmet: String) {
        PredmetRepository.odaberiGrupu(predmet)
    }

    fun vratiGrupu(): String{
        return PredmetRepository.vratiGrupu()
    }

    fun upisiGrupuUBazu(context: Context, nazivGrupe: String, predmet: String){
         scope.launch {
            var grupe = PredmetIGrupaRepository.getGrupe().filter { grupa -> grupa.naziv == nazivGrupe }
             PredmetIGrupaRepository.upisiUGrupu(grupe.first().id)
             AccountRepository.postaviVrijemeZadnjegUpdate(context, trenutniKorisnik)
             for(grupica in grupe) {
                 grupica.nazivPredmeta = predmet
                 PredmetIGrupaRepository.upisiGrupuUBazu(context, grupica)
             }
         }
    }

    fun upisiPredmetUBazu(context: Context, predmet: String){
        scope.launch {
            var upisani = PredmetIGrupaRepository.getPredmeti().filter { pred -> pred.naziv == predmet }.first()
            PredmetIGrupaRepository.upisiPredmetUBazu(context, upisani)
        }
    }
}