package ba.etf.rma21.projekat

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import ba.etf.rma21.projekat.data.repositories.AccountRepository.acHash
import ba.etf.rma21.projekat.view.FragmentKvizovi
import ba.etf.rma21.projekat.view.FragmentPoruka
import ba.etf.rma21.projekat.view.FragmentPredmeti
import ba.etf.rma21.projekat.viewmodel.AccountListViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjaOdgovoriViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PokusajMainViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView

//"http://rma21-etf.herokuapp.com/account/c0a0a01c-06f8-4879-ab7e-0a0fca6fe984"
class MainActivity : AppCompatActivity() {
    private lateinit var bottomNav: BottomNavigationView
    private lateinit  var pokusajMainViewModel: PokusajMainViewModel
    private  var pitanjeKvizListViewModel = PitanjeKvizListViewModel()
    private  var accountListViewModel = AccountListViewModel()


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.kvizovi -> {
                val favoritesFragment = FragmentKvizovi.newInstance()
                openFragment(favoritesFragment)
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                val recentFragments = FragmentPredmeti.newInstance()
                openFragment(recentFragments)
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false
                return@OnNavigationItemSelectedListener true
            }
            R.id.predajKviz -> {
                pitanjeKvizListViewModel.predajKviz()
                var bundle = Bundle()
                bundle.putString("kviz", trenutniKviz)
                val fragment = FragmentPoruka()
                fragment.arguments = bundle
                openFragment(fragment)
                pitanjeKvizListViewModel.predajOdgovore(trenutniKvizId)
                kvizListViewModel.azurirajKviz(AccountRepository.getContext(), trenutniKvizId)
                bottomNav.menu.findItem(R.id.kvizovi).isVisible = true
                bottomNav.menu.findItem(R.id.predmeti).isVisible = true
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false
                return@OnNavigationItemSelectedListener true
            }
            R.id.zaustaviKviz -> {
                val favoritesFragment = FragmentKvizovi.newInstance()
                openFragment(favoritesFragment)
                bottomNav.selectedItemId = R.id.kvizovi
                bottomNav.menu.findItem(R.id.kvizovi).isVisible = true
                bottomNav.menu.findItem(R.id.predmeti).isVisible = true
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNav= findViewById(R.id.bottomNav)
        bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        bottomNav.selectedItemId= R.id.kvizovi
        val predmetsFragment = FragmentKvizovi.newInstance()
        openFragment(predmetsFragment)

        pokusajMainViewModel = ViewModelProviders.of(this)[PokusajMainViewModel::class.java]

        accountListViewModel.postaviContext(this)

        val uri = intent
        if(uri != null) {
            val hash = uri.getStringExtra("payload")
            if(hash != null && hash != acHash)
               accountListViewModel.postaviHash(hash)
            else{
                accountListViewModel.postaviStariHash(acHash)
            }
        }


        pokusajMainViewModel.selected.observe(this, Observer<Boolean> { item ->
            if(item == false){
                bottomNav.menu.findItem(R.id.kvizovi).isVisible = false
                bottomNav.menu.findItem(R.id.predmeti).isVisible = false
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = true
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = true
            }
            else {
                bottomNav.menu.findItem(R.id.kvizovi).isVisible = true
                bottomNav.menu.findItem(R.id.predmeti).isVisible = true
                bottomNav.menu.findItem(R.id.predajKviz).isVisible = false
                bottomNav.menu.findItem(R.id.zaustaviKviz).isVisible = false
            }
        })
    }

    private fun openFragment(fragment: androidx.fragment.app.Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        if(bottomNav.selectedItemId == R.id.kvizovi) {
            bottomNav.selectedItemId = R.id.kvizovi
            pokusajMainViewModel.omoguciMain()
        }
        else {
            bottomNav.selectedItemId = R.id.kvizovi
        }
    }
}

