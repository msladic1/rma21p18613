package ba.etf.rma21.projekat.view

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProviders
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PokusajMainViewModel
import ba.etf.rma21.projekat.viewmodel.scope
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.launch


class FragmentPokusaj (val listaPitanja: List<Pitanje>?, val kvizTakenId: Int = 0) : Fragment() {
    private lateinit var pokusajMainViewModel: PokusajMainViewModel
    private lateinit var navView: NavigationView
    private  var pitanjeKvizListViewModel = PitanjeKvizListViewModel()

    private val mOnNavigationItemSelectedListener = NavigationView.OnNavigationItemSelectedListener { item ->
        scope.launch {
            val id: Int = item.itemId
            if (id < listaPitanja!!.size) {
                val pokusaj = FragmentPitanje(listaPitanja!!.elementAt(id), kvizTakenId)
                otvoriPitanje(pokusaj)

            }
            if (id == listaPitanja.size) {
                var bundle = Bundle()
                bundle.putString("kviz", trenutniKviz)
                //bundle.putString("predmet", predmet.selectedItem.toString())
                val fragment = FragmentPoruka()
                fragment.arguments = bundle
                otvoriPitanje(fragment)
            }
        }
        return@OnNavigationItemSelectedListener true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  {
        var view = inflater.inflate(R.layout.pokusaj_fragment, container, false)

        pokusajMainViewModel = activity?.run { ViewModelProviders.of(this)[PokusajMainViewModel::class.java] } ?: throw Exception("Invalid Activity")
        var pom: Int = 0
        for(pitanjeKviz: PitanjeKviz in predaniKvizovi){
            for(pitanje: Pitanje in listaPitanja!!){
                var trenutak = PitanjeKviz(pitanje.naziv, trenutniKviz, trenutniPredmet)
                if(trenutak.toString() == pitanjeKviz.toString())
                    pom++
            }
        }

        pokusajMainViewModel.onemoguciMain()

        navView = view.findViewById(R.id.navigacijaPitanja)
        navView.setNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val menuPitanja: Menu = navView.menu
        var id: Int = 0
        for(i in listaPitanja!!.indices) {
            menuPitanja.add(0, i, i + 1, (i + 1).toString())
            id++
        }

        if(predan){
            for(pitanje: Pitanje in listaPitanja!!)
                pitanjeKvizListViewModel.dodajPredanKviz(PitanjeKviz(pitanje.naziv, trenutniKviz, trenutniPredmet))
            predan = false
        }

        var zavrseni = true
        var pitanjaKviz: MutableList<PitanjeKviz> = emptyList<PitanjeKviz>().toMutableList()

        if(predaniKvizovi.isNotEmpty()) {
            for (pitanje: Pitanje in listaPitanja) {
                pitanjaKviz.add(PitanjeKviz(pitanje.naziv, trenutniKviz, trenutniPredmet))
            }
        }
        else {
            zavrseni = false
        }

        if(pitanjaKviz.isNotEmpty()) {
            for(pitkvi: PitanjeKviz in predaniKvizovi) {
                    pitanjaKviz.remove(pitkvi)
                }
            zavrseni = pitanjaKviz.isEmpty()
        }

        if(zavrseni){
            menuPitanja.add(0,id,id+1,"Rezultat")
            pokusajMainViewModel.omoguciMain()
        }
        else
            menuPitanja.removeItem(id)


        return view
    }

     fun otvoriPitanje(pokusaj: Fragment){
        val transaction = fragmentManager!!.beginTransaction()
        transaction.replace(R.id.framePitanje, pokusaj)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}