package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PredmetRepository {
    companion object {

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        fun getUpisani(): List<Predmet> {
            return mojiPredmeti
        }

        fun getAll(): List<Predmet> {
            return sviPredmeti()
        }
        // TODO: Implementirati i ostale potrebne metode

        fun getPredmetByGodina(godina: Int): List<Predmet> {
            return sviPredmeti().filter { predmet -> predmet.godina.equals(godina) }
        }

        fun getPredmetByNaziv(naziv: String): List<Predmet> {
            return sviPredmeti().filter { predmet -> predmet.naziv.equals(naziv) }
        }

        fun setMyPredmeti(naziv: String){
            mojiPredmeti(naziv)
        }

        fun odaberiGodinu(godina: String) {
            izmjeniPosljednjuGodinu(godina)
        }

        fun vratiGodinu(): String{
            return vratiPosljednjuGodinu()
        }

        fun odaberiPredmet(predmet: String) {
            izmjeniPosljednjiPredmet(predmet)
        }

        fun vratiPredmet(): String{
            return vratiPosljednjiPredmet()
        }

        fun odaberiGrupu(predmet: String) {
            izmjeniPosljednjuGrupu(predmet)
        }

        fun vratiGrupu(): String{
            return vratiPosljednjuGrupu()
        }

        suspend fun getPredmeteIzBaze(context: Context):List<Predmet>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var predmeti = db!!.predmetDao().getUpisane()
                return@withContext predmeti
            }
        }

        suspend fun upisiPredmetUBazu(context: Context, predmet: Predmet): String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.predmetDao().insertPredmet(predmet)
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }
    }

}