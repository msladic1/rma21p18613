package ba.etf.rma21.projekat.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PokusajMainViewModel: ViewModel() {
    val selected = MutableLiveData<Boolean>()

    fun onemoguciMain() {
        selected.value = false
    }

    fun omoguciMain() {
        selected.value = true
    }

}