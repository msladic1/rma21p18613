package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDao {
    @Query("SELECT * FROM pitanje")
    suspend fun getPitanjaZaKviz(): List<Pitanje>
    @Insert
    suspend fun insertPitanje(vararg pitanje: Pitanje)
    @Query("DELETE FROM pitanje")
    suspend fun deleteAll()
}