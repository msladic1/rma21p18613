package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.graphics.Movie
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

//import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository.Companion.dodajZapocetiKvi


class PitanjeKvizRepository {
    companion object {
        /*fun getPitanja(nazivKviza: String, nazivPredmeta: String?): List<Pitanje> {
            var pitanja: MutableList<Pitanje> = emptyList<Pitanje>().toMutableList()
            var pom: List<PitanjeKviz> = svaPitanjaKviza().filter { pitanje -> pitanje.kviz == nazivKviza && pitanje.predmet == nazivPredmeta }
            for (kvizPitanje in pom)
                pitanja.add(svaPitanja().first { pitanje -> pitanje.naziv == kvizPitanje.naziv })
            return pitanja
        }*/

        fun dodajZapocetiKvi(kviz: PitanjeKviz, pozicijaTacnost: HashMap<Int, Boolean>) {
            dodajZapocetiKviz(kviz, pozicijaTacnost)
        }

        fun postaviTrenutni(kviz: String){
            postaviTrenutniKviz(kviz)
        }

        fun postaviTrenutniPred(predmet: String?) {
            postaviTrenutniPredmet(predmet)
        }

        fun predajKviz(){
            predaj()
        }

        fun dodajPredanKviz(kviz: PitanjeKviz){
            dodajPredaniKviz(kviz)
        }

        fun dodajTacni(){
            povecaj()
        }

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPitanja(idKviza:Int):List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getPitanjZakViz(idKviza)
                return@withContext response
            }
        }

        suspend fun getPitanjaIzBaze(context: Context):List<Pitanje>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var pitanja = db!!.pitanjeDao().getPitanjaZaKviz()
                return@withContext pitanja
            }
        }

        suspend fun upisiPitanjaUBazu(context: Context, pitanja: List<Pitanje>): String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    for(pitanje in pitanja)
                        db!!.pitanjeDao().insertPitanje(pitanje)
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }
    }

}