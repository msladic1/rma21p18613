//package ba.etf.rma21.projekat
//
//import android.app.Activity
//import android.content.Intent
//import android.os.Bundle
//import android.view.Menu
//import android.view.MotionEvent
//import android.view.View
//import android.widget.AdapterView
//import android.widget.ArrayAdapter
//import android.widget.Button
//import android.widget.Spinner
//import androidx.appcompat.app.AppCompatActivity
//import androidx.core.app.ActivityCompat.recreate
//import ba.etf.rma21.projekat.data.models.Grupa
//import ba.etf.rma21.projekat.data.models.Predmet
//import ba.etf.rma21.projekat.data.models.mojiPredmeti
//import ba.etf.rma21.projekat.viewmodel.GrupaListViewModel
//import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
//import ba.etf.rma21.projekat.viewmodel.PredmetListViewModel
//import ba.etf.rma21.projekat.viewmodel.scope
//import kotlinx.coroutines.launch
//
//class UpisPredmet : AppCompatActivity()  {
//    private lateinit var godina: Spinner
//    private lateinit var predmet: Spinner
//    private lateinit var grupa: Spinner
//    private lateinit var dodajPredmetDugme: Button
//    private var predmetListViewModel = PredmetListViewModel()
//    private var grupaListViewModel = GrupaListViewModel()
//    private lateinit var adapterPredmeti : ArrayAdapter<String>
//    private lateinit var adapterGrupe : ArrayAdapter<String>
//    private var listaPredmeta = arrayListOf<Predmet>()
//    private var listaGrupa = arrayListOf<Grupa>()
//    private var brojGodine : Int = 0
//    private var predmeti = mutableListOf<String>()
//    private var grupe = mutableListOf<String>()
//    private var nazivPredmeta : String = ""
//    private lateinit var odabranaGodina : String
//    private var brojKlikovaGodina : Int = 0
//    private var brojKlikovaPredmet : Int = 0
//    private var brojKlikovaGrupa : Int = 0
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_upis_predmet)
//
//        godina = findViewById(R.id.odabirGodina)
//        predmet = findViewById(R.id.odabirPredmet)
//        grupa = findViewById(R.id.odabirGrupa)
//        dodajPredmetDugme = findViewById(R.id.dodajPredmetDugme)
//
//        ArrayAdapter.createFromResource(
//                this,
//                R.array.godine,
//                android.R.layout.simple_spinner_item
//        ).also { adapter ->
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//            godina.adapter = adapter
//        }
//
//        odabranaGodina = intent.getStringExtra("odabranaGodina")!!
//        val x : Int = odabranaGodina.toInt() - 1
//        godina.setSelection(x)
//
//        adapterPredmeti = ArrayAdapter(this, android.R.layout.simple_spinner_item, predmeti)
//        adapterGrupe = ArrayAdapter(this, android.R.layout.simple_spinner_item, grupe)
//
//        godina.setOnTouchListener(object : View.OnTouchListener {
//            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//                when (event?.action) {
//                    MotionEvent.ACTION_DOWN -> brojKlikovaGodina += 1
//                }
//
//                return v?.onTouchEvent(event) ?: true
//            }
//        })
//
//        predmet.setOnTouchListener(object : View.OnTouchListener {
//            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//                when (event?.action) {
//                    MotionEvent.ACTION_DOWN -> brojKlikovaPredmet += 1
//                }
//
//                return v?.onTouchEvent(event) ?: true
//            }
//        })
//
//        grupa.setOnTouchListener(object : View.OnTouchListener {
//            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//                when (event?.action) {
//                    MotionEvent.ACTION_DOWN -> brojKlikovaGrupa += 1
//                }
//
//                return v?.onTouchEvent(event) ?: true
//            }
//        })
//
//        godina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                brojGodine = parent.getItemAtPosition(position).toString().toInt()
//                scope.launch {
//                    listaPredmeta =
//                        predmetListViewModel.getPredmetByGodina(brojGodine) as ArrayList<Predmet>
//                }
//
//                predmeti.removeAll(predmeti)
//
//                for(predmet: Predmet in listaPredmeta) {
//                    listaPredmeta.removeAll {predmet -> predmet.godina != brojGodine}
//                    predmeti.add(predmet.naziv)
//                    if(predmet in predmetListViewModel.getUpisani())
//                        predmeti.removeAll { naziv -> naziv == predmet.naziv}
//                    adapterPredmeti.notifyDataSetChanged()
//                }
//
//                adapterPredmeti.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                adapterPredmeti.notifyDataSetChanged()
//                predmet.adapter = adapterPredmeti
//
//                predmet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//                    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
//                        nazivPredmeta = parent.getItemAtPosition(position).toString()
//                        listaGrupa = grupaListViewModel.getGroupsByPredmet(nazivPredmeta) as ArrayList<Grupa>
//
//                        grupe.removeAll(grupe)
//
//                        for(grupa: Grupa in listaGrupa) {
//                            listaGrupa.removeAll {grupa -> grupa.nazivPredmeta != nazivPredmeta}
//                            grupe.add(grupa.naziv)
//                            adapterGrupe.notifyDataSetChanged()
//                        }
//
//                        adapterGrupe.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                        adapterGrupe.notifyDataSetChanged()
//                        grupa.adapter = adapterGrupe
//                    }
//                    override fun onNothingSelected(parent: AdapterView<*>?) {
//
//                    }
//                }
//            }
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//
//            }
//        }
//
//        dodajPredmetDugme.setOnClickListener {
//            if(brojKlikovaGodina > 0 && brojKlikovaPredmet > 0 && brojKlikovaGrupa > 0) {
//                if (predmet.selectedItem != null) {
//                    val intent = Intent()
//                    intent.putExtra("godina", godina.selectedItem.toString())
//                    intent.putExtra("predmet", predmet.selectedItem.toString())
//                    intent.putExtra("grupa", grupa.selectedItem.toString())
//                    setResult(Activity.RESULT_OK, intent)
//                    finish()
//                }
//            }
//        }
//    }
//
//    override fun onBackPressed() {
//        val intent = Intent()
//        intent.putExtra("godina", godina.selectedItem.toString())
//        setResult(Activity.RESULT_OK, intent)
//        finish()
//        super.onBackPressed()
//
//    }
//}
