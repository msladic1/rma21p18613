package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class OdgovorRepository {

    companion object {

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getOdgovoriKviz(idKviza:Int):List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                val poceti = TakeKvizRepository.getPocetiKvizovi()!!.filter { poceti -> poceti.KvizId == idKviza }.first()
                var response = ApiAdapter.retrofit.getOdgovori(AccountRepository.acHash, poceti.id)
                return@withContext response
            }
    }

        suspend fun postaviOdgovorKviz(idKvizTaken:Int,idPitanje:Int,odgovor:Int):Int {
            return withContext(Dispatchers.IO) {
                val jsonObject = JSONObject()

                var poceti =TakeKvizRepository.getPocetiKvizovi()!!.filter { kviz -> kviz.id == idKvizTaken }.first()
                var nesto = TakeKvizRepository.getPocetiKvizovi()
                var zapoceti = nesto!!.filter { zapoceti -> zapoceti.id == idKvizTaken }.first()
                var idKviza = zapoceti.KvizId
                var brojPitanja: Double = PitanjeKvizRepository.getPitanja(idKviza)!!.size.toDouble()
                var pitanje = PitanjeKvizRepository.getPitanja(idKviza)!!.filter { pitanje -> pitanje.id == idPitanje }.first()
                var broj: Double = (1/brojPitanja * 100)

                try{
                    var db = AppDatabase.getInstance(AccountRepository.getContext())
                    var sviOdgovori = db!!.odgovorDao().getOdgovorene(idKviza)
                    var broj = sviOdgovori.size
                    if(sviOdgovori.isNotEmpty())
                        sviOdgovori = sviOdgovori.filter { odgovor -> odgovor.PitanjeId == idPitanje }
                    if(sviOdgovori.isEmpty()) {
                        db!!.odgovorDao().insertOdgovor(Odgovor(broj+1, odgovor, idKvizTaken, idPitanje, idKviza))
                        db!!.pitanjeDao().insertPitanje(pitanje)
                    }
                }
                catch(error:Exception){
                }
                if(pitanje.tacan == odgovor)
                    return@withContext poceti.osvojeniBodovi.toInt() + broj.toInt()

                return@withContext poceti.osvojeniBodovi.toInt()
            }
        }

        suspend fun predajOdgovore(idKviz: Int) {
            return withContext(Dispatchers.IO) {
                val jsonObject = JSONObject()

                var db = AppDatabase.getInstance(AccountRepository.getContext())
                var odgovori = db!!.odgovorDao().getOdgovorene(idKviz)
                var poceti = TakeKvizRepository.getPocetiKvizovi()!!.filter { poce -> poce.KvizId == idKviz }.first()
                var brojPitanja: Double = PitanjeKvizRepository.getPitanja(idKviz)!!.size.toDouble()
                var broj: Double = (1/brojPitanja * 100)

                for(odgovor in odgovori) {
                    var pitanje = PitanjeKvizRepository.getPitanja(idKviz)!!.filter { pitanje -> pitanje.id == odgovor.PitanjeId }.first()
                    jsonObject.put("odgovor", odgovor.odgovoreno)
                    jsonObject.put("pitanje", odgovor.PitanjeId)

                    if (pitanje.tacan == odgovor.odgovoreno)
                        jsonObject.put("bodovi", poceti.osvojeniBodovi.toInt() + broj.toInt())
                    else
                        jsonObject.put("bodovi", poceti.osvojeniBodovi.toInt())
                    val jsonObjectString = jsonObject.toString()
                    val body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObjectString)
                    try {
                        var response = ApiAdapter.retrofit.postaviOdgovorKviz(AccountRepository.acHash, poceti.id, body)
                    } catch (e: Exception) {

                    }
                }
            }
        }

        suspend fun getOdgovoreIzBaze(context: Context, idKviza: Int):List<Odgovor>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var odgovori = db!!.odgovorDao().getOdgovorene(idKviza)
                return@withContext odgovori
            }
        }

        suspend fun upisiOdgovorUBazu(context: Context, odgovor: Odgovor): String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.odgovorDao().insertOdgovor(odgovor)
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }
    }
}