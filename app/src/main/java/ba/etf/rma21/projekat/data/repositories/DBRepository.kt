package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.trenutniKorisnik
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import java.util.*

class DBRepository {
    companion object{
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun updateNow():Boolean?{
            return withContext(Dispatchers.IO) {
                var a = trenutniKorisnik
                var response = ApiAdapter.retrofit.getUpdate(AccountRepository.getHash(), trenutniKorisnik.lastUpdate)
                if (response.changed != null && response.changed!!) {
                    return@withContext true
                }
                return@withContext false
            }
        }
    }
}