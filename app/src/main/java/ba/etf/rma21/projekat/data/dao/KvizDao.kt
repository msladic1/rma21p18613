package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface KvizDao {
    @Query("SELECT * FROM kviz")
    suspend fun getUpisane(): List<Kviz>
    @Insert
    suspend fun insertKviz(vararg kviz: Kviz)
    @Query("DELETE FROM kviz")
    suspend fun deleteAll()
    @Query("UPDATE kviz SET predan = 1 WHERE id == :kvizeId")
    suspend fun updateKviz(kvizeId: Int)
}