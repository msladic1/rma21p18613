package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.core.graphics.toColorInt
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.viewmodel.PitanjaOdgovoriViewModel
import ba.etf.rma21.projekat.viewmodel.PitanjeKvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PokusajMainViewModel
import ba.etf.rma21.projekat.viewmodel.scope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FragmentPitanje(val pitanje: Pitanje, val kvizTakenId: Int = 0) : Fragment() {
    private lateinit var textView: TextView
    private lateinit var listView: ListView
    private var pitanjeKvizListViewModel = PitanjeKvizListViewModel()
    private var trenutno: PitanjeKviz = PitanjeKviz(pitanje.naziv, trenutniKviz, trenutniPredmet)
    private var odgovoriNaKvizu = mutableListOf<Odgovor>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.pitanje_fragment, container, false)
        super.onViewCreated(view, savedInstanceState)

        textView = view.findViewById(R.id.tekstPitanja)
        listView = view.findViewById(R.id.odgovoriLista)
        listView.isEnabled = true

        textView.text = pitanje.tekstPitanja
        val adapter = ArrayAdapter<String>(activity!!.baseContext, android.R.layout.simple_list_item_1, pitanje.opcije)

        listView.adapter = adapter


        for (pitanjeKviz in zapocetiKvizovi.keys) {
                if (trenutno.toString() == pitanjeKviz.toString()) {
                    val zap = zapocetiKvizovi.getValue(pitanjeKviz)

                    if (zap.containsValue(true)) {
                        val odgovor = pitanje.tacan
                        listView.isEnabled = true
                        Handler().postDelayed({
                            listView.performItemClick(listView.getChildAt(odgovor), odgovor, listView.adapter.getItemId(odgovor))
                        }, 1)
                    } else {
                        val tacan = pitanje.tacan
                        val odgovor = listView.getItemIdAtPosition(zap.keys.first()).toInt()
                        listView.isEnabled = true
                        Handler().postDelayed({
                            listView.performItemClick(listView.getChildAt(tacan), tacan, listView.adapter.getItemId(tacan))
                            listView.performItemClick(listView.getChildAt(odgovor), odgovor, listView.adapter.getItemId(odgovor))
                        }, 1)
                    }

                    listView.isEnabled = false
                }
            }

            for (pitanjeKviz in predaniKvizovi) {
                if (trenutno.toString() == pitanjeKviz.toString())
                    listView.isEnabled = false
            }


        listView.setOnItemClickListener { parent, view1, position, id ->
                scope.launch {
                    if (position == pitanje.tacan) {
                        val tv: TextView = view1 as TextView
                        tv.setTextColor("#3DDC84".toColorInt())
                        pitanjeKvizListViewModel.dodajZapocetiKviz(PitanjeKviz(pitanje.naziv, trenutniKviz, trenutniPredmet), hashMapOf(position to true))
                        pitanjeKvizListViewModel.dodajTacni()
                    } else {
                        var tv: TextView = view1 as TextView
                        tv.setTextColor("#DB4F3D".toColorInt())
                        pitanjeKvizListViewModel.dodajZapocetiKviz(PitanjeKviz(pitanje.naziv, trenutniKviz, trenutniPredmet), hashMapOf(position to false))
                        listView.performItemClick(listView.getChildAt(pitanje.tacan), pitanje.tacan, listView.getItemIdAtPosition(pitanje.tacan))
                    }
                    pitanjeKvizListViewModel.postaviOdgovor(kvizTakenId, pitanje.id, position)
                    listView.isEnabled = false
                }
        }
        return view
    }

    fun onSuccess(odgovori: List<Odgovor>){
        GlobalScope.launch (Dispatchers.IO){
            withContext(Dispatchers.Main){
               odgovoriNaKvizu = odgovori.toMutableList()
            }
        }
    }
}