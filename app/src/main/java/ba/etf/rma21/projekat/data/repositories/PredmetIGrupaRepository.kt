package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.models.trenutniKorisnik
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


class PredmetIGrupaRepository {
    //private val student: AccountRepository = AccountRepository()

    companion object {

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPredmeti(): List<Predmet> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllPredmeti()
                return@withContext response
            }
        }

        suspend fun getGrupe(): List<Grupa> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllGrupa()
                return@withContext response
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta:Int): List<Grupa> {
            return withContext(Dispatchers.IO) {
                var sviPredmeti : List<Predmet> = getPredmeti()
                sviPredmeti = sviPredmeti.filter { predmet -> predmet.id == idPredmeta  }
                var response = ApiAdapter.retrofit.getGrupaZaPredmet(idPredmeta)
                for(grupa in response){
                    grupa.nazivPredmeta = sviPredmeti[0].naziv
                }
                return@withContext response
            }
        }

        suspend fun getIdGrupeZaKviz(idKviza:Int): List<Int> {
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getGrupaZaKviz(idKviza)
                var sviId = arrayListOf<Int>()
                for(grupa in response)
                    sviId.add(grupa.id)
                return@withContext sviId
            }
        }

        suspend fun upisiUGrupu(idGrupa:Int):Boolean?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.upisiUGrupu(idGrupa, AccountRepository.acHash)
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
                val date: String = sdf.format(Date().minutes - 1)
                trenutniKorisnik.lastUpdate = date
                if(response.toString().contains("dodan"))
                    return@withContext true
                return@withContext false
            }
        }

        suspend fun getUpisaneGrupe():List<Grupa>?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getMojeGrupe(AccountRepository.acHash)
                return@withContext response
            }
        }

        suspend fun getGrupeIzBaze(context: Context):List<Grupa>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var grupe = db!!.grupaDao().getUpisane()
                return@withContext grupe
            }
        }

        suspend fun upisiGrupuUBazu(context: Context, grupa: Grupa): String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.grupaDao().insertGrupa(grupa)
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }
        suspend fun upisiPredmetUBazu(context: Context, predmet: Predmet): String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.predmetDao().insertPredmet(predmet)
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }
    }
}