package ba.etf.rma21.projekat.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils.indexOf
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.viewmodel.GrupaListViewModel
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.PredmetListViewModel
import ba.etf.rma21.projekat.viewmodel.scope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FragmentPredmeti : Fragment(){
    private lateinit var godina: Spinner
    private lateinit var predmet: Spinner
    private lateinit var grupa: Spinner
    private lateinit var dodajPredmetDugme: Button
    private var predmetListViewModel = PredmetListViewModel()
    private var kvizListViewModel = KvizListViewModel()
    private var grupaListViewModel = GrupaListViewModel()
    private lateinit var adapterPredmeti : ArrayAdapter<String>
    private lateinit var adapterGrupe : ArrayAdapter<String>
    private var listaPredmeta = arrayListOf<Predmet>()
    private var listaGrupa = arrayListOf<Grupa>()
    private var brojGodine : Int = 0
    private var predmeti = mutableListOf<String>()
    private var grupe = mutableListOf<String>()
    private var nazivPredmeta : String = ""
    private lateinit var odabranaGodina : String
    private lateinit var odabraniPredmet : String
    private lateinit var odabranaGrupa : String
    private var brojKlikovaGodina : Int = 0
    private var brojKlikovaPredmet : Int = 0
    private var brojKlikovaGrupa : Int = 0
    private lateinit var sviKvizoviAdapter: KvizListAdapter

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.predmeti_fragment, container, false)

            godina = view.findViewById(R.id.odabirGodina)
            predmet = view.findViewById(R.id.odabirPredmet)
            grupa = view.findViewById(R.id.odabirGrupa)
            dodajPredmetDugme = view.findViewById(R.id.dodajPredmetDugme)

            sviKvizoviAdapter = KvizListAdapter(arrayListOf()) {null}

            ArrayAdapter.createFromResource(
                activity!!.baseContext,
                R.array.godine,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                godina.adapter = adapter
            }

            adapterPredmeti =
                ArrayAdapter(activity!!.baseContext, android.R.layout.simple_spinner_item, predmeti)
            adapterGrupe =
                ArrayAdapter(activity!!.baseContext, android.R.layout.simple_spinner_item, grupe)


            odabranaGodina = predmetListViewModel.vratiGodinu()
            godina.setSelection(odabranaGodina.toInt() - 1)

            godina.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                    when (event?.action) {
                        MotionEvent.ACTION_DOWN -> brojKlikovaGodina += 1
                    }

                    return v?.onTouchEvent(event) ?: true
                }
            })

            predmet.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                    when (event?.action) {
                        MotionEvent.ACTION_DOWN -> brojKlikovaPredmet += 1
                    }

                    return v?.onTouchEvent(event) ?: true
                }
            })

            grupa.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                    when (event?.action) {
                        MotionEvent.ACTION_DOWN -> brojKlikovaGrupa += 1
                    }

                    return v?.onTouchEvent(event) ?: true
                }
            })

            godina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    predmetListViewModel.odaberiGodinu(godina.selectedItem.toString())
                    brojGodine = parent.getItemAtPosition(position).toString().toInt()

                    scope.launch {
                        listaPredmeta = predmetListViewModel.getPredmetByGodina(brojGodine) as ArrayList<Predmet>
                        predmeti.removeAll(predmeti)

                        for (predmet: Predmet in listaPredmeta) {
                            listaPredmeta.removeAll { predmet -> predmet.godina != brojGodine }
                            predmeti.add(predmet.naziv)
                            if (predmet in predmetListViewModel.getUpisani())
                                predmeti.removeAll { naziv -> naziv == predmet.naziv }
                            adapterPredmeti.notifyDataSetChanged()
                        }

                        adapterPredmeti.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        adapterPredmeti.notifyDataSetChanged()
                        predmet.adapter = adapterPredmeti
                        odabraniPredmet = predmetListViewModel.vratiPredmet()
                        predmet.setSelection(predmeti.indexOf(odabraniPredmet))
                    }

                    predmet.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>,
                            view: View,
                            position: Int,
                            id: Long
                        ) {
                            predmetListViewModel.odaberiPredmet(
                                parent.getItemAtPosition(position).toString()
                            )
                            nazivPredmeta = parent.getItemAtPosition(position).toString()
                            var idPredmeta = listaPredmeta.filter { predmet -> predmet.naziv == nazivPredmeta }.first().id

                            scope.launch {
                                listaGrupa =
                                    grupaListViewModel.getGroupsByPredmet(idPredmeta) as ArrayList<Grupa>

                                grupe.removeAll(grupe)

                                for (grupa: Grupa in listaGrupa) {
                                    listaGrupa.removeAll { grupa -> grupa.nazivPredmeta != nazivPredmeta }
                                    grupe.add(grupa.naziv)
                                    adapterGrupe.notifyDataSetChanged()
                                }

                                adapterGrupe.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                adapterGrupe.notifyDataSetChanged()
                                grupa.adapter = adapterGrupe
                                odabranaGrupa = predmetListViewModel.vratiGrupu()
                                grupa.setSelection(grupe.indexOf(odabranaGrupa))
                            }

                            grupa.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(
                                        parent: AdapterView<*>,
                                        view: View,
                                        position: Int,
                                        id: Long
                                    ) {
                                        predmetListViewModel.odaberiGrupu(
                                            parent.getItemAtPosition(
                                                position
                                            ).toString()
                                        )
                                    }

                                    override fun onNothingSelected(parent: AdapterView<*>?) {

                                    }
                                }
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {

                        }
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {

                }
            }


            dodajPredmetDugme.setOnClickListener {
                if (brojKlikovaGodina > 0 && brojKlikovaPredmet > 0 && brojKlikovaGrupa > 0) {
                    if (predmet.selectedItem != null) {
                        kvizListViewModel.setMyKvizes(grupa.selectedItem.toString())
                        predmetListViewModel.setMyPredmeti(predmet.selectedItem.toString())
                    }
                    val porukasFragment = FragmentPoruka()
                    openFragment(porukasFragment)
                    context?.let {
                        //kvizListViewModel.getMyKvizes1(it, ::onSuccess)
                        predmetListViewModel.upisiGrupuUBazu(it, grupa.selectedItem.toString(), predmet.selectedItem.toString())
                        predmetListViewModel.upisiPredmetUBazu(it, predmet.selectedItem.toString())
                    }
                }
            }

        return view
    }

    fun onSuccess(kvizevi: List<Kviz>){
        GlobalScope.launch (Dispatchers.IO){
            withContext(Dispatchers.Main){
                sviKvizoviAdapter.updateKvizovi(kvizevi)
            }
        }
    }

    fun onSuccessGrupe(grupice: List<Grupa>){
        GlobalScope.launch (Dispatchers.IO){
            withContext(Dispatchers.Main){
                return@withContext grupice
            }
        }
    }

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
    }

    private fun openFragment(fragment: Fragment) {
        var bundle = Bundle()
        bundle.putString("grupa", grupa.selectedItem.toString())
        bundle.putString("predmet", predmet.selectedItem.toString())
        fragment.arguments = bundle
        val transaction = fragmentManager!!.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}