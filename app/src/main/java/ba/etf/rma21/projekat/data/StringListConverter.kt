package ba.etf.rma21.projekat.data

import androidx.room.TypeConverter

class StringListConverter {
    @TypeConverter
    fun izStringa(stringListString: String): List<String> {
        return stringListString.split(",").map { it }
    }

    @TypeConverter
    fun uString(stringList: List<String>): String {
        return stringList.joinToString(separator = ",")
    }
}