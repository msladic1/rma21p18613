package ba.etf.rma21.projekat.data.models

import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import ba.etf.rma21.projekat.viewmodel.scope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
val sdf = SimpleDateFormat("yyyy-mm-dd")
val kvizListViewModel = KvizListViewModel()


var mojiKvizovi: MutableList<Kviz> = kvizListViewModel.getUpisani().toMutableList()



suspend fun mojiKvizovi(grupa: String =""): List<Kviz>{
    val ggrupa = PredmetIGrupaRepository.getGrupe().filter { grupa1 -> grupa1.naziv == grupa }
    val idGrupe = ggrupa[0].id
    PredmetIGrupaRepository.upisiUGrupu(idGrupe)
    val pom: List<Kviz> = KvizRepository.getAll().filter { kviz -> kviz.nazivGrupe!!.split(", ").toList().contains(idGrupe.toString())}
    for(kviz: Kviz in pom)
        mojiKvizovi.add(kviz)

    return mojiKvizovi
}
