package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account

@Dao
interface AccountDao {
    @Query("SELECT * FROM account")
    suspend fun getAll(): List<Account>
    @Insert
    suspend fun insertAll(vararg account: Account?)
    @Delete
    suspend fun deleteAll(vararg account: Account?)
    @Query ("DELETE FROM account")
    suspend fun obrisi()
}