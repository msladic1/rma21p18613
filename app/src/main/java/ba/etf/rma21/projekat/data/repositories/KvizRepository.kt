package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*

class KvizRepository {
    companion object {
        // TODO: Implementirati
        init {
            // TODO: Implementirati
        }

        val sdf = SimpleDateFormat("yyyy-mm-dd")

        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

         suspend fun getMyKvizes1(): List<Kviz> {
            return getUpisani().sortedBy { kviz -> kviz.datumPocetka }
        }

        fun getMyKvizes(): List<Kviz> {
            return mojiKvizovi
        }

        suspend fun getAll():List<Kviz>{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getAllKviz()
                for(odg in response)
                    odg.nazivPredmeta = ""
                for(i in 0 until response.size){
                    val grupeZaKviz = PredmetIGrupaRepository.getIdGrupeZaKviz(response[i].id)
                    response[i].nazivGrupe = grupeZaKviz.joinToString (", ")
                    val sviPredmeti = PredmetIGrupaRepository.getPredmeti()
                    var grupeZaPredmete = listOf<Grupa>()
                    for(predmet in sviPredmeti){
                        grupeZaPredmete = PredmetIGrupaRepository.getGrupeZaPredmet(predmet.id)
                        var predmeti = mutableListOf<String>()
                        for(grupa in grupeZaPredmete){
                            if(grupa.id in grupeZaKviz){
                                predmeti.add(predmet.naziv)
                            }
                        }
                        predmeti = predmeti.distinct().toMutableList()
                        var upomoc = predmeti.joinToString(", ")
                        response[i].nazivPredmeta += "$upomoc "
                    }

                }
                return@withContext response
            }
        }

        suspend fun getById(id:Int):Kviz?{
            return withContext(Dispatchers.IO) {
                var response = ApiAdapter.retrofit.getKvizById(id)
                if(response.id == 0)
                    return@withContext null
                return@withContext response
            }
        }

        suspend fun getUpisani():List<Kviz> {
            return withContext(Dispatchers.IO) {
                var sviKvizovi = listOf<Kviz>()
                var grupe = PredmetIGrupaRepository.getUpisaneGrupe()
                var pom = mutableListOf<Kviz>()

                for (grupica in grupe!!) {
                    val nesto = getAll().filter { kviz -> kviz.nazivGrupe!!.split(", ").toList().contains(grupica.id.toString())}
                    for(nekaj in nesto)
                        pom.add(nekaj)
                }
                sviKvizovi = pom.toList()
                return@withContext sviKvizovi
            }
        }

        suspend fun getDone(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                var uradjeniKvizovi: List<Kviz> = getUpisani()
                return@withContext uradjeniKvizovi.filter { kviz -> kviz.datumRada != null }
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        suspend fun getFuture(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                var uradjeniKvizovi: List<Kviz> = getUpisani()
                return@withContext uradjeniKvizovi.filter { kviz -> sdf.parse(kviz.datumPocetka) > Date() }
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        suspend fun getNotTaken(): List<Kviz> {
            return withContext(Dispatchers.IO) {
                var uradjeniKvizovi: List<Kviz> = getUpisani()
                return@withContext uradjeniKvizovi.filter { kviz -> sdf.parse(kviz.datumKraj!!) < Date() && kviz.datumRada == null }
            }
        }
        // TODO: Implementirati i ostale potrebne metode

        suspend fun setMyKvizes(grupa: String){
            mojiKvizovi(grupa)
        }

        suspend fun getKvizoveIzBaze(context: Context):List<Kviz>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var kvizovi = db!!.kvizDao().getUpisane()
                return@withContext kvizovi
            }
        }

        suspend fun azurirajKviz(context: Context, kvizId: Int) {
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                db!!.kvizDao().updateKviz(kvizId)
            }
        }

        suspend fun getIzBazeDone(context: Context):List<Kviz>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var kvizovi = db!!.kvizDao().getUpisane()
                return@withContext kvizovi.filter { kviz -> kviz.datumRada != null }
            }
        }

        suspend fun getIzBazeNotTaken(context: Context):List<Kviz>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var kvizovi = db!!.kvizDao().getUpisane()
                return@withContext kvizovi.filter { kviz -> kviz.datumKraj != null && sdf.parse(kviz.datumKraj!!) < Date() && kviz.datumRada == null }
            }
        }

        suspend fun getIzBazeFuture(context: Context):List<Kviz>?{
            return withContext(Dispatchers.IO) {
                var db = AppDatabase.getInstance(context)
                var kvizovi = db!!.kvizDao().getUpisane()
                return@withContext kvizovi.filter {  kviz -> sdf.parse(kviz.datumPocetka) > Date() }
            }
        }

        suspend fun upisiKvizUBazu(context: Context, kviz: Kviz): String?{
            return withContext(Dispatchers.IO) {
                try{
                    var db = AppDatabase.getInstance(context)
                    db!!.kvizDao().insertKviz(kviz)
                    return@withContext "success"
                }
                catch(error:Exception){
                    return@withContext null
                }
            }
        }
    }

}