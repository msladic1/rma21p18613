package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.trenutniKorisnik
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

object AccountRepository {
    var acHash : String = "c0a0a01c-06f8-4879-ab7e-0a0fca6fe984"

    private lateinit var context: Context
    fun setContext(_context: Context){
        context=_context
    }

    fun getContext(): Context{
        return context
    }

    suspend fun postaviHash(acHash1:String):Boolean{
        return withContext(Dispatchers.IO) {
            try {
                var a = getAccountIzBaze(getContext())
                if(a!!.acHash != acHash1) {
                    acHash = acHash1
                    if (obrisiSve(context)) {
                        acHash = acHash1
                        trenutniKorisnik.acHash = acHash1
                        postaviAccountUBazu(context, trenutniKorisnik)
                    }
                }
                return@withContext true
            }catch(e: Exception){
                return@withContext false
            }
        }
    }

    suspend fun postaviStariHash(acHash1:String):Boolean{
        return withContext(Dispatchers.IO) {
            try {
                acHash = acHash1
                trenutniKorisnik.acHash = acHash1
                postaviAccountUBazu(context, trenutniKorisnik)
                return@withContext true
            }catch(e: Exception){
                return@withContext false
            }
        }
    }

    suspend fun getHash():String {
        return withContext(Dispatchers.IO) {
            return@withContext acHash
        }
    }

    suspend fun getAccountIzBaze(context: Context):Account?{
        return withContext(Dispatchers.IO) {
            var db = AppDatabase.getInstance(context)
            var accounti = db!!.accountDao().getAll()
            if(accounti.isNotEmpty())
                return@withContext accounti.first()
            else return@withContext Account("","")
        }
    }

    suspend fun postaviVrijemeZadnjegUpdate(context: Context, account: Account?): String?{
        return withContext(Dispatchers.IO) {
            try{
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val date: String = sdf.format(Date())
                var db = AppDatabase.getInstance(context)
                db!!.accountDao().deleteAll(account)
                account!!.lastUpdate = date
                db!!.accountDao().insertAll(account)
                return@withContext "success"
            }
            catch(error:Exception){
                return@withContext null
            }
        }
    }

    suspend fun postaviAccountUBazu(context: Context, account: Account?): String?{
        return withContext(Dispatchers.IO) {
            try{
                var db = AppDatabase.getInstance(context)
                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                var c: Calendar = Calendar.getInstance()
                c.time = Date()
                c.add(Calendar.MINUTE, -1)
                val date: String = sdf.format(c.time)
                db!!.accountDao().obrisi()
                db!!.accountDao().deleteAll(account)
                account!!.lastUpdate = date
                db!!.accountDao().insertAll(account)
                return@withContext "success"
            }
            catch(error:Exception){
                return@withContext null
            }
        }
    }

    suspend fun obrisiSve(context1: Context): Boolean{
        return withContext(Dispatchers.IO) {
            try{
                var db = AppDatabase.getInstance(context1)
                var db2 = AppDatabase.getInstance(context)
                db!!.grupaDao().deleteGrupe()
                db2!!.grupaDao().deleteGrupe()
                db!!.predmetDao().deleteAll()
                db2!!.predmetDao().deleteAll()
                db!!.odgovorDao().deleteAll()
                db2!!.odgovorDao().deleteAll()
                db!!.pitanjeDao().deleteAll()
                db2!!.pitanjeDao().deleteAll()
                db!!.kvizDao().deleteAll()
                db2!!.kvizDao().deleteAll()
                return@withContext true
            }
            catch(error:Exception){
                return@withContext false
            }
        }
    }
}