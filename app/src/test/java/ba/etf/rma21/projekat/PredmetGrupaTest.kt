package ba.etf.rma21.projekat

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import junit.framework.Assert.assertEquals
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.hamcrest.CoreMatchers.`is` as Is
import org.junit.Test

class PredmetGrupaTest {
    @Test
    fun getPrvaGodina(){
        val predmeti = PredmetRepository.getPredmetByGodina(1)
        assertEquals(predmeti.size,4)
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("MPS"))))
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("PŠP"))))
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("VIS"))))
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("IF1"))))
        assertThat(predmeti, not(hasItem<Predmet>(hasProperty("naziv", Is("PAD")))))
    }
    @Test
    fun getDrugaGodina(){
        val predmeti = PredmetRepository.getPredmetByGodina(2)
        assertEquals(predmeti.size,2)
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("PAD"))))
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("IEK"))))
        assertThat(predmeti, not(hasItem<Predmet>(hasProperty("naziv", Is("MPS")))))
    }
    @Test
    fun getTrecaGodina(){
        val predmeti = PredmetRepository.getPredmetByGodina(3)
        assertEquals(predmeti.size,2)
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("RG"))))
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("VPM"))))
        assertThat(predmeti, not(hasItem<Predmet>(hasProperty("naziv", Is("PC")))))
    }

    @Test
    fun getCetvrtaGodina(){
        val predmeti = PredmetRepository.getPredmetByGodina(4)
        assertEquals(predmeti.size,2)
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("SS"))))
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("VČ"))))
        assertThat(predmeti, not(hasItem<Predmet>(hasProperty("naziv", Is("PAD")))))
    }

    @Test
    fun getPetaGodina(){
        val predmeti = PredmetRepository.getPredmetByGodina(5)
        assertEquals(predmeti.size,2)
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("PC"))))
        assertThat(predmeti, hasItem<Predmet>(hasProperty("naziv", Is("PKS"))))
        assertThat(predmeti, not(hasItem<Predmet>(hasProperty("naziv", Is("PAD")))))
    }

    @Test
    fun getGrupe1(){
        val grupe = GrupaRepository.getGroupsByPredmet("MPS")
        assertEquals(grupe.size,2)
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("MPS1-1"))))
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("MPS1-2"))))
        assertThat(grupe, not(hasItem<Grupa>(hasProperty("naziv", Is(nullValue())))))
    }

    @Test
    fun getGrupe2(){
        val grupe = GrupaRepository.getGroupsByPredmet("IF1")
        assertEquals(grupe.size,2)
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("IF11-1"))))
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("IF11-2"))))
        assertThat(grupe, not(hasItem<Grupa>(hasProperty("naziv", Is("IF11-3")))))
    }

    @Test
    fun getGrupe3(){
        val grupe = GrupaRepository.getGroupsByPredmet("PC")
        assertEquals(grupe.size,2)
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("PC5-1"))))
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("PC5-2"))))
        assertThat(grupe, hasItem<Grupa>(hasProperty("nazivPredmeta", Is("PC"))))
        assertThat(grupe, not(hasItem<Grupa>(hasProperty("naziv", Is("PC1-5")))))
    }

    @Test
    fun getGrupe4(){
        val grupe = GrupaRepository.getGroupsByPredmet("RG")
        assertEquals(grupe.size,2)
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("RG3-1"))))
        assertThat(grupe, hasItem<Grupa>(hasProperty("naziv", Is("RG3-2"))))
        assertThat(grupe, not(hasItem<Grupa>(hasProperty("nazivPredmeta", Is("PC")))))
        assertThat(grupe, not(hasItem<Grupa>(hasProperty("naziv", Is("RG3-3")))))
    }
}