//package ba.etf.rma21.projekat
//
//import ba.etf.rma21.projekat.data.models.Kviz
//import ba.etf.rma21.projekat.data.repositories.KvizRepository
//import junit.framework.Assert.assertEquals
//import org.hamcrest.MatcherAssert.assertThat
//import org.hamcrest.Matchers.*
//import org.hamcrest.CoreMatchers.`is` as Is
//import org.junit.Test
//
//class KvizFilterTest {
//    @Test
//    fun testGetMyKvizes(){
//        val kvizovi = KvizRepository.getMyKvizes()
//        assertEquals(kvizovi.size,4)
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("MPS"))))
//        assertThat(kvizovi, not(hasItem<Kviz>(hasProperty("nazivPredmeta", Is("SS")))))
//    }
//    @Test
//    fun testGetAllkvizes(){
//        val kvizovi = KvizRepository.getAll()
//        assertEquals(kvizovi.size,25)
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("VIS"))))
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivGrupe", Is("VIS1-1"))))
//        assertThat(kvizovi, not(hasItem<Kviz>(hasProperty("nazivPredmeta", Is("IM1")))))
//    }
//    @Test
//    fun testGetDoneKvizes(){
//        val kvizovi = KvizRepository.getDone()
//        assertEquals(kvizovi.size,2)
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("MPS"))))
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivGrupe", Is("MPS1-1"))))
//        assertThat(kvizovi, not(hasItem<Kviz>(hasProperty("nazivGrupe", Is("MPS1-2")))))
//        assertThat(kvizovi, not(hasItem<Kviz>(hasProperty("osvojeniBodovi",  Is(nullValue()))))) //Uradjeni kviz mora imati osvojene bodove
//    }
//
//    @Test
//    fun testGetFutureKvizes(){
//        val kvizovi = KvizRepository.getFuture()
//        assertEquals(kvizovi.size,1)
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("IF1"))))
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivGrupe", Is("IF11-2"))))
//        assertThat(kvizovi, not(hasItem<Kviz>(hasProperty("nazivGrupe", Is("IF11-1")))))
//        assertThat(kvizovi, (hasItem<Kviz>(hasProperty("osvojeniBodovi",  Is(nullValue()))))) //Buduci kviz nema osvojene bodove
//    }
//
//    @Test
//    fun testGetProsliKvizovi(){
//        val kvizovi = KvizRepository.getNotTaken()
//        assertEquals(kvizovi.size,1)
//        assertThat(kvizovi, hasItem<Kviz>(hasProperty("nazivPredmeta", Is("PŠP"))))
//        assertThat(kvizovi, (hasItem<Kviz>(hasProperty("osvojeniBodovi",  Is(nullValue()))))) //Neuradjeni kviz nema osvojene bodove
//    }
//}